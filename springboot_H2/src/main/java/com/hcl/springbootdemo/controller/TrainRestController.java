package com.hcl.springbootdemo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.springbootdemo.entity.Train;
import com.hcl.springbootdemo.service.ITrainService;

@RestController
@RequestMapping("train/v1")
public class TrainRestController {

	
		@Autowired
		ITrainService service;
	
	@PostMapping(value = "/add",consumes = "application/json",produces = "application/json")
	public  Train  insert(@RequestBody  Train train) {
		
		return service.addTrain(train);
		
		
		
	}
	
	@GetMapping(value="/getall",produces = "application/json")
	public List<Train>  getAll(){
		
		return  service.getAllTrain();
		
	}
	
	@PutMapping(value="/update",consumes = "application/json")
	public Train  update(@RequestBody Train train) {
		
		return service.updateTrain(train);
		
	}
	
	@GetMapping("/get/{id}")
	public  Train  getTrainById(@PathVariable int id) {
		
	return  service.getTrainById(id);
		
		}	
	@DeleteMapping("/remove/{id}")
	public String  remove(@PathVariable int id) {
		
	service.getTrainById(id);
	
		return "Record deleted...";
		
	}
	
	
	
	
}
