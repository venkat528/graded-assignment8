package com.hcl.springbootdemo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="Train_Info")
public class Train {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	//private Long  id;
	@Column(name="train_name")
	//private String name;
	//private double marks;
	
	private int id;
	private String name;
	private String source;
	private String destination;

}
