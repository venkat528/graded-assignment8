package com.hcl.springbootdemo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.springbootdemo.entity.Train;
import com.hcl.springbootdemo.repository.TrainRepository;

@Service
public class TrainServiceImp  implements  ITrainService  {

		@Autowired
		TrainRepository repo;
	
	
	@Override
	public Train addTrain(Train train) {
		// TODO Auto-generated method stub
		return   repo.save(train);
	}


	@Override
	public List<Train> getAllTrain() {
		// TODO Auto-generated method stub
		return repo.findAll();
	}


	@Override
	public Train updateTrain(Train train) {
		// TODO Auto-generated method stub
		return repo.save(train);
	}
	
	
	

	@Override
	public Train getTrainById(int id) {
		// TODO Auto-generated method stub
		return getTrainById(id);
	}


	

	

	

	



	
}
