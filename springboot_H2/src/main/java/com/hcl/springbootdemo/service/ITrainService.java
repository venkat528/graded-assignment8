package com.hcl.springbootdemo.service;

import java.util.List;

import com.hcl.springbootdemo.entity.Train;

public interface ITrainService {
	
	
	public Train  addTrain(Train train);
	
	
	

	//public Train deleteTrain(Train train);
	public List<Train> getAllTrain();

	public Train updateTrain(Train train);

	public Train getTrainById(int id);




	

	
	

}
