package com.hcl.springbootdemo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.springbootdemo.entity.Train;
@Repository
public interface TrainRepository  extends JpaRepository<Train, Long>  {

	
	
}
