package com.hcl.springbootdemo.exception;

public class CutsomException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CutsomException(String message) {
		super(message);
	}

}
