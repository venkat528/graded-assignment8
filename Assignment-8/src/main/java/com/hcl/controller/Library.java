
package com.hcl.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.demo.repository.DatabaseConnection;
import com.hcl.entity.Books;
import com.hcl.entity.Login;

@Controller
public class Library {

	@Autowired
	public DatabaseConnection bookDatabase;

	@GetMapping("/logins")
	public String getLogin() {
		return "login";
	}

	@PostMapping("/logins")
	public String postLogin(Login user, Map<String, String> map, Map<String, List<Books>> map1,
			HttpSession session) {

		boolean a = bookDatabase.loginUser(user);
		// System.out.println(a);
		if (a) {
			List<Books> books = this.bookDatabase.getAllBooks();
			map1.put("books", books);
			map.put("email", user.getEmail());
			session.setAttribute("email", user.getEmail());
			return "dashboard";
		} else {
			return "login";
		}
	}

	@GetMapping("/register")
	public String getRegister() {
		return "register";
	}

	@PostMapping("/register")
	public String postRegister(Login user) {
		System.out.println(user);
		if (bookDatabase.registerData(user)) {
			return "login";
		}
		return "redirect:register?give_correct_details";
	}

	@GetMapping("/dashboard")
	public String getDashboard() {
		return "dashboard";
	}

	@PostMapping("/dashboard")
	public String postDashboard() {
		return "dashboard";
	}

	@GetMapping("/book")
	public String getBook(Map<String, String> map, Map<String, List<Books>> map1, HttpSession session) {
		List<Books> books = this.bookDatabase.getAllBooks();
		map1.put("books", books);
		return "book";
	}

	@PostMapping("/book")
	public String postBook(Map<String, String> map, Map<String, List<Books>> map1, HttpSession session) {
		List<Books> books = this.bookDatabase.getAllBooks();
		map1.put("books", books);
		return "book";
	}

	@GetMapping("/visit")
	public String getVisit1(Map<String, String> map, Map<String, List<Books>> map1, HttpSession session) {
		List<Books> books = this.bookDatabase.getVisitBooks();
		map1.put("books", books);
		return "visit";
	}

	@PostMapping("/visit")
	public String postVisit(Map<String, String> map, Map<String, List<Books>> map1, HttpSession session) {
		List<Books> books = this.bookDatabase.getVisitBooks();
		map1.put("books", books);
		return "visit";
	}

	@RequestMapping("/favorit")
	public String getFavorite1(@RequestParam int id, Map<String, List<Books>> map1) {
		bookDatabase.inserter(id);
		// map1.put("books", books);
		return "redirect:favorite";
	}

	@RequestMapping("/favorite")
	public String getFavorite(Map<String, List<Books>> map1) {
		List<Books> books = this.bookDatabase.getFavBooks();
		map1.put("books", books);
		return "favorite";
	}

	@RequestMapping("/readlate")
	public String getreadlater(@RequestParam int id, Map<String, List<Books>> map1) {
		bookDatabase.insert(id);
		// map1.put("books", books);
		return "redirect:readlater";
	}

	@RequestMapping("/readlater")
	public String getreadlater1(Map<String, List<Books>> map1) {
		List<Books> books = this.bookDatabase.getreadlateBooks();
		map1.put("books", books);
		return "readlater";
	}

	@GetMapping("/logout")
	public String getLogout() {
		return "login";
	}

	@PostMapping("/logout")
	public String postLogout1() {
		return "login";
	}
}
