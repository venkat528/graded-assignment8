package com.hcl.entity;

public class Login {

	private String name;
	private String country;
	private String email;
	private String phone;
	private String password;

	public Login() {

	}

	public Login(String email, String password)

	{
		this.email = email;
		this.password = password;

	}

	public Login(String name, String country, String email, String phone, String password) {
		super();
		this.name = name;
		this.country = country;
		this.email = email;
		this.phone = phone;
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getEmail() {
		return email;
	}

	public String setEmail(String email) {
		return this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPassword() {
		return password;
	}

	public String setPassword(String password) {
		return this.password = password;
	}

	@Override
	public String toString() {
		return "LoginUser [name=" + name + ", country=" + country + ", email=" + email + ", phone=" + phone
				+ ", password=" + password + "]";
	}

}
