<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<title>INDEX</title>
<link rel="stylesheet" type="text/css" href="slide navbar style.css">
<link
	href="https://fonts.googleapis.com/css2?family=Jost:wght@500&display=swap"
	rel="stylesheet">
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3"
	crossorigin="anonymous">
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
	integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js"
	integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"
	integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13"
	crossorigin="anonymous"></script>
<style type="text/css">
body {
	align-items: center;
	font-family: 'Jost', sans-serif;
	background: linear-gradient(0.25turn, #3f87a6, #ebf8e1, #f69d3c);
}

.main {
	width: 350px;
	height: 500px;
	background: red;
	overflow: hidden;
	background:
		url("https://doc-08-2c-docs.googleusercontent.com/docs/securesc/68c90smiglihng9534mvqmq1946dmis5/fo0picsp1nhiucmc0l25s29respgpr4j/1631524275000/03522360960922298374/03522360960922298374/1Sx0jhdpEpnNIydS4rnN4kHSJtU1EyWka?e=view&authuser=0&nonce=gcrocepgbb17m&user=03522360960922298374&hash=tfhgbs86ka6divo3llbvp93mg4csvb38")
		no-repeat center/cover;
	border-radius: 10px;
	box-shadow: 5px 20px 50px #000;
}

#h1 {
	color: white
}

p {
	color: white;
}

a {
	color: white;
}

.ae {
	color: white;
}

.col-4 {
	margin-bottom: 5%;
	padding-left: 5%;
}

h1 {
	color: white;
}

h5 {
	color: white;
}

.ae {
	padding-left: 90%;
}
</style>

</head>
<body>
	<br>
	<div class="row container-fluid">
		<div class="col">
			<h1>WELCOME TO LIBRARY MANAGEMENT</h1>
			
		</div>
		<div class="col" style="text-align: right;">
			<div class='aes'>

				<a href='register'><button type="button"
						class="btn btn-outline-light">Register Here</button></a>
			</div>


			
		</div>
		<div class="col-1" style="text-align: right;">
			<div class='aes'>
				<a href='logins'><button type="button"
						class="btn btn-outline-light">Login</button></a>
			</div>


			<p class='aes'>Registered user</p>
		</div>
	</div>

	<div class='container'>
		<center>
			<div class='aes'>
				<a href='visit'><button type="button"
						class="btn btn-outline-light">LIST OF BOOKS</button></a></a>
			</div>

		</center>

	</div>
	<div id="carouselExampleIndicators" class="carousel slide"
		data-bs-ride="carousel">
		<div class="carousel-indicators">
			<button type="button" data-bs-target="#carouselExampleIndicators"
				data-bs-slide-to="0" class="active" aria-current="true"
				aria-label="Slide 1"></button>
			<button type="button" data-bs-target="#carouselExampleIndicators"
				data-bs-slide-to="1" aria-label="Slide 2"></button>
			<button type="button" data-bs-target="#carouselExampleIndicators"
				data-bs-slide-to="2" aria-label="Slide 3"></button>
			<button type="button" data-bs-target="#carouselExampleIndicators"
				data-bs-slide-to="3" aria-label="Slide 4"></button>
			<button type="button" data-bs-target="#carouselExampleIndicators"
				data-bs-slide-to="4" aria-label="Slide 5"></button>
		</div>
		<div class="carousel-inner">
			<div class="carousel-item active">
				<img
					src="https://media.istockphoto.com/photos/empty-wooden-tables-in-public-library-picture-id1193273154?k=20&m=1193273154&s=612x612&w=0&h=CD6HpP_4VfdS2vTjEdunzWicSqQ28TsWFORoZquLqhg="
					width="30" height="425" class="d-block w-100" alt="...">
			</div>
			<div class="carousel-item">
				<img
					src="https://www.google.com/search?q=library+images&sxsrf=APq-WBsu7gc1M0hqcaW4cOfqApHUYxLwJQ:1643810369573&source=lnms&tbm=isch&sa=X&ved=2ahUKEwjT5p_pluH1AhWF7XMBHUyFAcMQ_AUoAXoECAEQAw&biw=1280&bih=569&dpr=1.5#imgrc=YP_kojBXbPTZ_M"
					width="80" height="400" class="d-block w-100" alt="...">
			</div>
			<div class="carousel-item">
				<img
					src="https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.mentalfloss.com%2Farticle%2F559404%2Fexpert-tips-and-tricks-organizing-your-home-library&psig=AOvVaw2NSZi9moRuwDGS4DTmv2mk&ust=1643896779895000&source=images&cd=vfe&ved=0CAsQjRxqFwoTCMD69e-W4fUCFQAAAAAdAAAAABAV"
					width="30" height="400" class="d-block w-100" alt="...">
			</div>
			<div class="carousel-item">
				<img
					src="https://images.indianexpress.com/2018/05/library.jpg"
					width="50" height="400" class="d-block w-100" alt="...">
			</div>
			<div class="carousel-item">
				<img
					src="https://upload.wikimedia.org/wikipedia/commons/7/72/NYC_-_New_York_City_Library_-_1723.jpg"
					width="50" height="425" class="d-block w-100" alt="...">
			</div>
		</div>
		<button class="carousel-control-prev" type="button"
			data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
			<span class="carousel-control-prev-icon" aria-hidden="true"></span> <span
				class="visually-hidden">Previous</span>
		</button>
		<button class="carousel-control-next" type="button"
			data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
			<span class="carousel-control-next-icon" aria-hidden="true"></span> <span
				class="visually-hidden">Next</span>
		</button>
	</div>
</body>
</html>