create database week8book;
use week8book;

create table Books(
id int(10) auto_increment primary key,
Title varchar(50),
Authorname varchar(30),
Genre varchar(30),
Price float);

insert into Books values(1,"The Lord of the Rings","J. R. R. Tolkien"," Novel, High fantasy","4000");
insert into Books values(2,"War and Peace"," Leo Tolstoy","Novel, Historical Fiction","265");
insert into Books values(3,"The Sun Also Rises","Ernest Hemingway","Novel, Historical Fiction","235");
insert into Books values(4,"The Stranger","Albert Camus","Novel, Absurdist fiction","500");
insert into Books values(5,"The Call of the Wild","Jack London","Novel, Adventure fiction","600");
insert into Books values(6,"The Color Purple","Alice Walker"," Novel, Epistolary novel","500");
insert into Books values(7,"Ulysses","James Joyce","Novel, Fiction","800");
insert into Books values(8,"Nineteen Eighty-Four","George Orwell"," Science fiction","275");
insert into Books values(9,"The Great Gatsby","F. Scott Fitzgerald","Novel, Fiction","600");
insert into Books values(10,"Pride and Prejudice","Jane Austen","Romance novel","400");


create table Login(
id int(10) auto_increment primary key,
name varchar(20),
phone varchar(10),
email varchar(30),
password varchar(10),
country varchar(10));

select * from Login;

create table Favorites(
id int(10) auto_increment primary key,
Title varchar(50),
Authorname varchar(30),
Genre varchar(30),
Price float);

select * from Favorites;

create table ReadLater(
id int(10) auto_increment primary key,
Title varchar(50),
Authorname varchar(30),
Genre varchar(30),
Price float);

select * from ReadLater;

create table favorite(
rid int auto_increment primary key,
id int(10),
foreign key(id) references books(id)
);

select * from favorite;

create table readlate(
rid int auto_increment primary key,
id int(10),
foreign key(id) references books(id)
);

select * from readlate;


