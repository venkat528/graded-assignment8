package com.hcl.maps;

import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.maps.entity.Department;
import com.hcl.maps.entity.Employee;
//import com.hcl.mapping.DepartmentRepository;
//import com.hcl.mapping.EmployeeRepository;
import com.hcl.maps.entity.Manufactures;
import com.hcl.maps.entity.Model;
import com.hcl.maps.reposotiory.DepartmentRepository;
import com.hcl.maps.reposotiory.EmployeeRepository;
import com.hcl.maps.reposotiory.ManufacturesRepo;
import com.hcl.maps.reposotiory.ModelRepo;
import org.springframework.data.repository.CrudRepository;

@SpringBootApplication
public class OneToManyApplication implements CommandLineRunner {
	
	 @Autowired
	    private ManufacturesRepo manufacturesRepo;
	   
	    @Autowired
	    private ModelRepo modelRepo;
	    
	    @Autowired
	    private EmployeeRepository employeeRepo;
	   
	    @Autowired
	    private DepartmentRepository deptRepo;
	    
	 

	public static void main(String[] args) {
		SpringApplication.run(OneToManyApplication.class, args);
	}


	@Override
	public void run(String... args) throws Exception {
		
		
		    /*Manufactures data=new Manufactures(1,"Honda");
		    
		    // Inserting the record in the Manufactures table.
		    manufacturesRepo.save(data);
		    
		        // Now try to mapped above record with multiple models
		        Model model1=new Model(1,"AYZ",data);
		        Model model2=new Model(2,"ZET",data);
		        modelRepo.save(model1);
		        modelRepo.save(model2);
		        */
		        
		        
				  Department dept = new Department();
				  
				  dept.setDepartName("Development");
				  
				  
				  Set<Employee> set = new HashSet<Employee>();
				  
				  Employee e1 = new Employee();
				  e1.setEmpName("king");
				  Employee e2 = new Employee();
				  e2.setEmpName("tom"); 
				  Employee e3 = new Employee();
				  e3.setEmpName("smith");
				  
				  set.add(e1); set.add(e2); set.add(e3);
				  
				  
				  
				  dept.setEmployees(set);
				  
				  
				  deptRepo.save(dept);
				 
		
	}

}
