package com.hcl.maps.reposotiory;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.maps.entity.Model;

public interface ModelRepo extends JpaRepository<Model, Integer> {

}
