package com.hcl.maps.reposotiory;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.maps.entity.Department;

@Repository
public interface DepartmentRepository  extends JpaRepository<Department, Integer>{

}
