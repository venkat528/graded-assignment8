package com.hcl.maps.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@NoArgsConstructor
@Data
@Entity

public class Employee {
	
	@Id
	@GeneratedValue
	private int empId;
	private String empName;
	
	
	  @ManyToOne(fetch = FetchType.EAGER)
	  
	  @JoinColumn(name="dept_no")
	  Department department;
	 

}
