package com.hcl.mappings;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.hcl.mappings.entites.Address;
import com.hcl.mappings.entites.Person;
import com.hcl.mappings.repositories.AddressRepository;
import com.hcl.mappings.repositories.PersonRepository;

@SpringBootApplication
public class OneToOneApplication implements CommandLineRunner {

	
	@Autowired
	private AddressRepository addressRepo;
	@Autowired
	private PersonRepository personRepo;
	
	public static void main(String[] args) {
		SpringApplication.run(OneToOneApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		
	
		 Person person = new Person(); 
		 person.setName("Dhana");
		 person.setAge(23);
		 
		 
		  Address address = new Address();
		  address.setCity("Hyderbad");
		  address.setState("Telegana");
		  address.setPincode(523240);
		  
		  addressRepo.save(address); 
		  
		  
		 // person.setAddress(address); 
		  
		  personRepo.save(person); 
		 

		
		
	}

}
