package com.hcl.mappings.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.mappings.entites.Person;

@Repository
public interface PersonRepository extends JpaRepository<Person, Integer> {

}
