package com.hcl.mappings.entites;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@ToString
public class Address {

	
	
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	
	private int addressId;
	private String city;
	private  String state;
	private  int pincode;
	
	@OneToOne(mappedBy = "address",fetch = FetchType.EAGER)
	private Person person;
	
	
	
	
	
}
