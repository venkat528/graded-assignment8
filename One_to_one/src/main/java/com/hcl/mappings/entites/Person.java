package com.hcl.mappings.entites;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@ToString
public class Person {

	@GeneratedValue
	@Id
	private int studentId;
	private String name;
	private int age;
	@OneToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)                      
	@JoinColumn(name="aid")   
	private Address address;
	
	
}
